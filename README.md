[[_TOC_]]

# Rust TOML config

Basic rust library used later on for reading a config file for a GitLab api token for a future utility. Mostly a basic exercise in creating a library and creating a descrete test file. 

# Rust Documents

Additional comments and documentation have been added through the Rust Documentation Engine. To generate to view/use, do the following: 
```bash
cargo doc --open
```

# Testing

To test not only the tests but verify the documentation run: 
```bash
cargo test
```
