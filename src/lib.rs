use serde::{Deserialize, Serialize};
use std::fmt;

mod test;  // Tests for the library

/// Config struct showing how to access GitLab access level 
/// Example
/// ```rust
/// use toml_creds::{Config, GitLabAccess};
/// let glvals = GitLabAccess{
///    apitoken: "https://gitlab.com".to_string(),
///    url: "glpat-6nTyQ9FFFFFzqsGT53Jo".to_string(),
/// };  
/// let mut empty_config = Config {
///     gitlab: glvals,
/// };  
/// ```
/// Associated example TOML
/// ```TOML
/// [gitlab]
/// url = "https://gitlab.com"
/// apitoken = "glpat-6nTyQ9FFFFFzqsGT53Jo"
/// ```
#[derive(Serialize, Deserialize, Debug)]
pub struct Config{
    pub gitlab: GitLabAccess,
}

/// GitLab Acces contents
///   
/// Example
/// ```rust
/// use toml_creds::GitLabAccess;
///
/// let glvals = GitLabAccess{
///     apitoken: "https://gitlab.com".to_string(),
///     url: "glpat-6nTyQ9FFFFFzqsGT53Jo".to_string(),
/// };
/// ```
/// Associated example TOML
/// ```TOML
/// [gitlab]
/// url = "https://gitlab.com"
/// apitoken = "glpat-6nTyQ9FFFFFzqsGT53Jo"
/// ```
#[derive(Serialize, Deserialize, Debug)]
pub struct GitLabAccess{
    /// GitLab URL to connect with
    pub url: String,
    /// GitLab api token (needs API read access only)
    pub apitoken: String,
}

impl fmt::Display for Config {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({}, {})", self.gitlab.url, self.gitlab.apitoken)
        // write!(f, "({}, {})", self.url, self.token)
    }
}

/// Used for readinging TOML file and producing the GitLab Credentials
/// 
/// Example
/// ```rust
///    use toml_creds::read_config;
///    const CONFIG_PATH: &str = "src/test.toml";
///     
///    match read_config(CONFIG_PATH.to_string()){
///         Ok(glc) => {
///             println!("Pass TOML read file");
///             println!("Show method: {}", glc.show());
///         },
///         Err(e) => {
///             println!("Fail! Please check the file path.");
///             println!("File source: {}\nError: {:?}", CONFIG_PATH, e);
///         },
///    };
/// ```
/// Associated example TOML
/// ```TOML
/// [gitlab]
/// url = "https://gitlab.com"
/// apitoken = "glpat-6nTyQ9FFFFFzqsGT53Jo"
/// ```
#[allow(dead_code)]
pub fn read_config(config_path: String) -> std::io::Result<Config> {
    let glfile = std::fs::read_to_string(config_path)?;
    // println!("config: \n{}", glfile);
    let glconfig : Config = toml::from_str(&glfile).unwrap();
    // println!("glconfig:\n{:#?}", glconfig); // Shows the entire object
    // println!("url in read: {}", glconfig.gitlab.url);
    Ok(glconfig)
}

impl Config {
    /// Set values with a classical set function
    /// Example
    /// ```rust
    /// use toml_creds::{Config, GitLabAccess};
    /// let glvals = GitLabAccess{
    ///    apitoken: "blank".to_string(),
    ///    url: "blank".to_string(),
    /// };  
    /// let mut empty_config = Config {
    ///     gitlab: glvals,
    /// };  
    /// empty_config.set(
    ///   "https://gitlab.com".to_string(), 
    ///   "abcMyToken123".to_string()
    /// ); 
    /// ```
    #[allow(dead_code)]
    pub fn set(&mut self, url: String, token: String) {
        self.gitlab.url = url;
        self.gitlab.apitoken = token;
    }

    /// Basic show function, please note the lack of space after the comma. Should only be used for diagnostic purposes.
	/// Example
    /// ```rust
    /// use toml_creds::{Config, GitLabAccess};
    /// let glvals = GitLabAccess{
    ///    apitoken: "https://gitlab.com".to_string(),
    ///    url: "abcMyToken123".to_string(),
    /// };  
    /// let mut empty_config = Config {
    ///     gitlab: glvals,
    /// };  
	/// empty_config.show();
    /// ```
    #[allow(dead_code)]
    pub fn show(&self) -> String {
        format!("({},{})", &self.gitlab.url, &self.gitlab.apitoken)
    }
}

