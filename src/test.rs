#[cfg(test)]
use super::*;


#[test]
fn test_config_set(){
    let glvals = GitLabAccess{
        apitoken: "blank".to_string(),
        url: "blank".to_string(),
    };  

    let mut empty_config = Config {
        gitlab: glvals,
    };  

    empty_config.set(
        "https://gitlab.com".to_string(), 
        "abcMyToken123".to_string()
    );  
    assert!(empty_config.show() == "(https://gitlab.com,abcMyToken123)");
}   

#[test]
fn read_toml_file(){
    const CONFIG_PATH: &str = "src/test.toml";
    
    match read_config(CONFIG_PATH.to_string()){
        Ok(glc) => {
            println!("Pass TOML read file");
            println!("Show method: {}", glc.show());
            assert!(glc.show() == "(https://gitlab.com,glpat-6nTyQ9FFFFFzqsGT53Jo)");
        },
        Err(e) => {
            println!("Fail! Please check the file path.");
            println!("File source: {}\nError: {:?}", CONFIG_PATH, e);
            assert!(1 == 2); // Force failed test
        },
    };
}
